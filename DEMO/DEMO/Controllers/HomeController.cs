﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ninject;

using EssentialTools2.Models;

namespace EssentialTools2.Controllers
{
    public class HomeController : Controller
    {
        private IValueCalculator calc;
        private Product[] products =
            {
                new Product {Name = "Kayak", Category = "Watersports", Price = 275M},
                new Product {Name = "LifeJacket", Category = "Watersports", Price = 48.95M},
                new Product {Name = "Soccer Ball", Category = "Soccer", Price = 19.50M},
                new Product {Name = "Corner Flag", Category = "Soccer", Price = 34.95M},
            };
        //Uses the NinjectDependencyResolver (Constructor injection)
        public HomeController(IValueCalculator calcParam)
        {
            calc = calcParam;
        }


        // GET: Home
        public ActionResult Index()
        {
            ShoppingCart cart = new ShoppingCart(calc) { Products = products };

            decimal totalValue = cart.CalculateProductTotal();

            return View(totalValue);
        }
    }
}