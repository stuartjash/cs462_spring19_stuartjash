﻿
// QUnit test for Calendar functionality
QUnit.module('Calendar Check Date');

const DAY = { "Sunday": 0, "Monday": 1, "Tuesday": 2, "Wednesday": 3, "Thursday": 4, "Friday": 5, "Saturday": 6 };

QUnit.test('Test valid string with normal format should parse OK and return Valid', function (assert) {
    // Arrange
    var inputDateString = "04/12/2018";
    // Act
    var output = checkDate(inputDateString);
    // Assert
    assert.ok(output.valid, inputDateString + "is a valid date");
    assert.equal(output.dayOfWeek, DAY.Thursday, "04/12/2018 is a Thursday");
    assert.equal(output.minDayOfWeek, DAY.Sunday, "April 1, 2018 is a Sunday");
    assert.equal(output.day, 12, inputDateString + " is the 12th of the month");
    assert.equal(output.maxDay, 30, "April 30th is the last day of the month");
    assert.equal(output.month, 4 - 1, "April is the 3rd month");
    assert.equal(output.year, 2018, inputDateString + " is in 2018");
});

QUnit.test('Test valid string with abbreviated format should parse OK and return Valid', function (assert) {
    // Arrange
    var inputDateString = "4/12/2018";
    // Act
    var output = checkDate(inputDateString);
    // Assert
    assert.ok(output.valid);
    assert.equal(output.dayOfWeek, DAY.Thursday);
    assert.equal(output.minDayOfWeek, DAY.Sunday);
    assert.equal(output.day, 12);
    assert.equal(output.maxDay, 30);
    assert.equal(output.month, 4 - 1);
    assert.equal(output.year, 2018);
});

// Example of failing tests. Requirements called for a full year but 
// it's natural to assume something like 4/12/18 should parse.  Failing
// tests can lead to new features, i.e. extending this to include
// short year.  
QUnit.test('Test valid string with abbreviated year format should parse OK and return Valid', function (assert) {
    // Arrange
    var inputDateString = "4/12/18";
    // Act
    var output = checkDate(inputDateString);
    // Assert
    assert.ok(output.valid);
    assert.equal(output.dayOfWeek, DAY.Thursday);
    assert.equal(output.minDayOfWeek, DAY.Sunday);
    assert.equal(output.day, 12);
    assert.equal(output.maxDay, 30);
    assert.equal(output.month, 4 - 1);
    assert.equal(output.year, 2018);
});

QUnit.test('Test invalid string should return NOT Valid', function (assert) {
    // Arrange
    var inputDateString = "4/q/eighteen";
    // Act
    var output = checkDate(inputDateString);
    // Assert
    assert.notOk(output.valid);
});

QUnit.module("Calendar Confirm Table construction");

QUnit.test("Test that calendar table is created for a valid date", function (assert) {
    // Arrange
    var inputDateString = "4/17/2018";
    var output = checkDate(inputDateString);
    // Act
    $("#theCalendar").append(calendarTable(output));
    // Assert
    var element = document.querySelector("#qunit-fixture div#theCalendar table.calendarTable");
    assert.ok(element);
});

QUnit.test("Test that calendar table is created for a valid date", function (assert) {
    // Arrange
    var inputDateString = "4/17/2018";
    var output = checkDate(inputDateString);
    // Act
    $("#theCalendar").append(calendarTable(output));

    // Assert
    var element = document.querySelector("#qunit-fixture div#theCalendar table.calendarTable");
    assert.ok(element.getElementsByClassName("today").length == 1);
    assert.equal(element.getElementsByClassName("today")[0].innerHTML, "17");
});