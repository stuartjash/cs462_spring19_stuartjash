/*
	Small example of using Javascript and jQuery to create a table, procedurally.  The user enters
	a date, which is validated and then used to generate a calendar for the month in which the 
	date is found.  
*/
/*global $, jQuery, alert*/

const months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];

/* Construct a table object usng the date information in dateObj (which is returned
   by checkDate). 
   Returns a jQuery table object containing the calendar. 
*/
function calendarTable(dateObj) {
    "use strict";
    // Create the main table object
    var tab = $("<table>", {
        "class": "calendarTable"
    });
    // Add a caption
    var tcapt = $("<caption>").html(months[dateObj.month] + " " + dateObj.year);
    tcapt.appendTo(tab);
    // and the table head
    var thead = $("<thead>\
			      <tr>\
			         <th>Sunday</th>\
					 <th>Monday</th>\
					 <th>Tuesday</th>\
					 <th>Wednesday</th>\
					 <th>Thursday</th>\
					 <th>Friday</th>\
					 <th>Saturday</th>\
			      </tr>\
				</thead>");
    thead.appendTo(tab);

    // The body is the main calendar.  We need to calculate where to start and stop
    // the days and how many weeks to include.
    var tbody = $("<tbody>");
    var trow = null;
    var tdat = null;
    var n = null;
    for (var ir = 0; ir < 6; ir++) {
        trow = $("<tr>");
        for (var ic = 0; ic < 7; ic++) {
            n = ir * 7 + (ic + 1) - dateObj.minDayOfWeek;
            if (n < 1 || n > dateObj.maxDay) {
                tdat = $("<td>");
            } else {
                if (n == dateObj.day)
                    tdat = $("<td>", {
                        id: n,
                        "class": "today"
                    }).html(n);
                else
                    tdat = $("<td>", {
                        id: n
                    }).html(n);
            }
            tdat.appendTo(trow);
        }
        trow.appendTo(tbody);
        if (n >= dateObj.maxDay)
            break;
    }
    tbody.appendTo(tab);
    return tab;
}

/* Validate and parse the date string input from the user.  Let's manually validate
   wiht a regex for the format: mm/dd/yyyy  or m/d/yyyy 
   Returns a custom object containing needed date and day, month, year data. 
*/
const dateRE = /\d{1,2}\/\d{1,2}\/\d{4}/;

function checkDate(date) {
    "use strict";
    var valid = dateRE.test(date);
    if (valid) {
        var d = new Date(date);
        // find number of first and last days of the month
        var dfirst = new Date(d.getFullYear(), d.getMonth(), 1);
        var dlast = new Date(d.getFullYear(), d.getMonth() + 1, 1);
        dlast.setDate(dlast.getDate() - 1);
        return {
            "valid": true,
            "dayOfWeek": d.getDay(), // 0 based: 0 == Sunday
            "minDayOfWeek": dfirst.getDay(),
            "day": d.getDate(), // days are 1 based
            "maxDay": dlast.getDate(),
            "month": d.getMonth(),
            "year": d.getFullYear()
        };
    } else
        return {
            "valid": false
        };
}

/* Attach an event function to the submit action of the form.  This works for clicking the submit 
   button or typing return. */
$("#generateCal").submit(function (event) {
    event.preventDefault();
    var dateString = $("#dateSelect").val().trim();
    // validate 
    var vobj = checkDate(dateString);
    if (vobj.valid) {
        console.log("Let's generate a calendar: dow = " + vobj.dayOfWeek + " day = " + vobj.day + " maxDay = " + vobj.maxDay + " minDOW = " + vobj.minDayOfWeek);
        $("#theCalendar").empty();
        $("#theCalendar").append(calendarTable(vobj));
        $("#dateEntry").html("");
    } else {
        console.log("Date string invalid: " + dateString);
        $("#theCalendar").empty();
        $("#dateEntry").html("Please enter a valid date");
    }
});