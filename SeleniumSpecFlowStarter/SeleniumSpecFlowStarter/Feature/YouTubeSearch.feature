﻿Feature: YouTubeSearch
	In order to test search functionality on youtube
	As a developer
	I want to ensure functionality is working end to end

	@scopedBinding
	Scenario: Youtube search for the given keyword should navigate to search
	Given I have navigated to the youtube website
	And I have entered India as search keyword
	When I press the search button
	Then I should navigate to search result page



