﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.WaitHelpers;
using System;
using TechTalk.SpecFlow;

namespace SeleniumSpecFlowStarter.StepBindings
{
    [Binding]
    public class YouTubeSearchSteps : IDisposable
    {
        private string searchKeyword;
        private ChromeDriver driver;

        public YouTubeSearchSteps()
        {
            driver = new ChromeDriver();
        }

        [Given(@"I have navigated to the youtube website")]
        public void GivenIHaveNavigatedToTheYoutubeWebsite()
        {
            driver.Navigate().GoToUrl("https://www.youtube.com");
            Assert.IsTrue(driver.Title.ToLower().Contains("youtube"));
        }

        [Given(@"I have entered (.*) as search keyword")]
        public void GivenIHaveEnteredIndiaAsAsearchKeyword(string searchString)
        {
            this.searchKeyword = searchString.ToLower();
            var searchInputBox = driver.FindElementById("search");
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(2));
            wait.Until(ExpectedConditions.ElementIsVisible(By.Id("search")));
            searchInputBox.SendKeys(searchKeyword);
        }

        [When(@"I press the search button")]
        public void WhenIPressTheSearchButton()
        {
            var searchButton = driver.FindElementByCssSelector("button#search-icon-legacy");
            searchButton.Click();
        }

        [Then(@"I should navigate to search result page")]
        public void ThenIShouldNavigateToSearchResultPage()
        {
            System.Threading.Thread.Sleep(2000);
            Assert.IsTrue(driver.Url.ToLower().Contains(searchKeyword));
            Assert.IsTrue(driver.Title.ToLower().Contains(searchKeyword));

        }

        public void Dispose()
        {
            if (driver != null)
            {
                driver.Dispose();
                driver = null;
            }


        }
    }
}
