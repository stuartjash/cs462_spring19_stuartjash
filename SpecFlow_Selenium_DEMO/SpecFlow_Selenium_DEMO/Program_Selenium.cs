﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using NUnit.Framework;

namespace SpecFlow_Selenium_DEMO
{
    class Program_Selenium
    {
        //static void Main(string[] args)
        //{

        //}
        //Create the reference for our browser
        IWebDriver driver = new ChromeDriver();
            
            [SetUp]
            public void Initialize()
            { 
                //Navigate to google page
                driver.Navigate().GoToUrl("http:www.google.com");
            }

            [Test]
            public void ExecuteTest()
            {
                //Find the Search text box UI Element
                IWebElement element = driver.FindElement(By.Name("q"));

                //Perform Ops
                element.SendKeys("executeautomation");

                //Console.WriteLine("Executed Test");
            }

            
            [TearDown]
            public void CleanUp()
            {
                driver.Close();
                //Console.WriteLine("Closed the browser");
            }
    }
}
